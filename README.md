# Demain sera _

Reflet d’une prise de conscience partagée des limites d’un système qui continue à détériorer notre environnement et à aggraver les inégalités, de nombreuses initiatives émergent, se développent et agissent pour tenter d’inverser cette tendance .

Notre initiative, Demain sera _ propose une réflexion sur le rôle des designers pour faciliter l’évolution vers un futur souhaité. Il nous semble en effet urgent d’accompagner les secteurs public et privé à prendre en compte et à mesurer l’impact de chacun de leurs projets.

Nous avons donc souhaité développer, en collectif ouvert, un accompagnement et des outils dédiés, pour donner des clés d’actions à toutes les équipes projets.

Cet espace permet la gestion du site du collectif. N'hésitez pas à participer.

-------

Cher collectif [le wiki vous attend](https://gitlab.com/demain-sera/demain-sera.gitlab.io/-/wikis/home) pour votre active collaboration.

Pour info, l'ensemble des tââches à effectuer sur le site sont rentrées sous forme d'[issues](https://gitlab.com/demain-sera/demain-sera.gitlab.io/-/issues).
